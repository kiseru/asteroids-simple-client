package com.kiseru.asteroids.client

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.kiseru.asteroids.client.auth.AuthResponseDto
import com.kiseru.asteroids.client.command.CommandResponseDto
import com.kiseru.asteroids.client.command.CommandType
import com.kiseru.asteroids.client.token.Token
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.takeWhile
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.awaitExchange
import java.io.InputStream
import java.io.OutputStream
import java.net.Socket
import java.util.Scanner

private const val EXIT_COMMAND = "exit"

private const val FINISH_COMMAND = "finish"

private const val HOST = "localhost"

private const val PORT = 6501

private const val DEFAULT_PASSWORD = "password"

private val log = LoggerFactory.getLogger("AsteroidsSimpleClient")

@SpringBootApplication
class AsteroidSimpleClient {

    @Bean
    fun socket() =
        Socket(HOST, PORT)

    @Bean
    fun webClient() =
        WebClient.create("http://localhost:8080")
}

suspend fun main(args: Array<String>): Unit = coroutineScope {
    val context = runApplication<AsteroidSimpleClient>(*args)
    val socket = context.getBean(Socket::class.java)
    val inputStream = socket.getInputStream()
    val outputStream = socket.getOutputStream()
    val token = authorize(inputStream, outputStream)
    val webClient = context.getBean(WebClient::class.java)
    launch {
        startReceiver(inputStream)
    }
    launch {
        startSender(webClient, token.username, DEFAULT_PASSWORD)
    }
}

suspend fun startReceiver(inputStream: InputStream) =
    serverResponses(inputStream)
        .onStart { log.info("Receiver started") }
        .takeWhile { it != EXIT_COMMAND && it != FINISH_COMMAND }
        .collect { println(it) }

private suspend fun serverResponses(inputStream: InputStream): Flow<String> =
    channelFlow {
        val reader = inputStream.bufferedReader()
        while (true) {
            send(reader.readLine())
        }
    }
        .flowOn(Dispatchers.IO)

suspend fun authorize(inputStream: InputStream, outputStream: OutputStream): AuthResponseDto {
    val reader = inputStream.bufferedReader()
    val writer = outputStream.bufferedWriter()
    for (i in 1..2) {
        val welcomeMessage = withContext(Dispatchers.IO) { reader.readLine() }
        println(welcomeMessage)
    }
    val username = userInputFlow()
        .take(1)
        .single()
    withContext(Dispatchers.IO) {
        writer.write(username)
        writer.newLine()
        writer.flush()
    }
    val response = withContext(Dispatchers.IO) { reader.readLine() }
    val objectMapper = jacksonObjectMapper()
    val token = objectMapper.readValue(response, Token::class.java).token
    return AuthResponseDto(username, token)
}

suspend fun startSender(
    webClient: WebClient,
    username: String,
    password: String,
) {
    val inputMap = createInputMap()
    userInputFlow()
        .onStart { log.info("Sender started") }
        .map { CommandType.valueOf(it) }
        .collect {
            val handler = inputMap[it]
            if (handler != null) {
                val response = handler(webClient, username, password)
                println(response.result)
                return@collect
            }

            println("Введена неизвестная команда")
        }
}

private fun createInputMap(): Map<CommandType, suspend (WebClient, String, String) -> CommandResponseDto> =
    mapOf(
        CommandType.DOWN to ::sendDownCommand,
        CommandType.EXIT to ::sendExitCommand,
        CommandType.GO to ::sendGoCommand,
        CommandType.IS_ASTEROID to ::sendIsAsteroidCommand,
        CommandType.IS_GARBAGE to ::sendIsGarbageCommand,
        CommandType.IS_WALL to ::sendIsWallCommand,
        CommandType.LEFT to ::sendLeftCommand,
        CommandType.RIGHT to ::sendRightCommand,
        CommandType.UP to ::sendUpCommand,
    )

private suspend fun sendDownCommand(webClient: WebClient, username: String, password: String): CommandResponseDto =
    sendCommand(webClient, "api/v1/commands/down", username, password)

private suspend fun sendExitCommand(webClient: WebClient, username: String, password: String): CommandResponseDto =
    sendCommand(webClient, "api/v1/commands/exit", username, password)

private suspend fun sendGoCommand(webClient: WebClient, username: String, password: String): CommandResponseDto =
    sendCommand(webClient, "api/v1/commands/go", username, password)

private suspend fun sendIsAsteroidCommand(
    webClient: WebClient,
    username: String,
    password: String,
): CommandResponseDto =
    sendCommand(webClient, "api/v1/commands/is-asteroid", username, password)

private suspend fun sendIsGarbageCommand(webClient: WebClient, username: String, password: String): CommandResponseDto =
    sendCommand(webClient, "api/v1/commands/is-garbage", username, password)

private suspend fun sendIsWallCommand(webClient: WebClient, username: String, password: String): CommandResponseDto =
    sendCommand(webClient, "api/v1/commands/is-wall", username, password)

private suspend fun sendLeftCommand(webClient: WebClient, username: String, password: String): CommandResponseDto =
    sendCommand(webClient, "api/v1/commands/left", username, password)

private suspend fun sendRightCommand(webClient: WebClient, username: String, password: String): CommandResponseDto =
    sendCommand(webClient, "api/v1/commands/right", username, password)

private suspend fun sendUpCommand(webClient: WebClient, username: String, password: String): CommandResponseDto =
    sendCommand(webClient, "api/v1/commands/up", username, password)

private suspend fun sendCommand(webClient: WebClient, uri: String, username: String, password: String): CommandResponseDto =
    webClient
        .post()
        .uri(uri)
        .headers { it.setBasicAuth(username, password) }
        .awaitExchange { it.awaitBody() }

suspend fun userInputFlow(): Flow<String> =
    channelFlow {
        val scanner = Scanner(System.`in`)
        while (true) {
            send(scanner.awaitNextLine())
        }
    }

suspend fun Scanner.awaitNextLine(): String =
    withContext(Dispatchers.IO) { nextLine() }
